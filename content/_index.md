## Hey All!

My name is Joe. I created this page to host some of my works as well as 
a collection of other topics that I would love to share (coming soon). This ranges from my acedemic
works as a graduate student at Michigan State University to fun projects I work
on withdata science and machine learning. You can also find me 
on [Github](https://github.com/tpjoe) and [Linkedin](https://www.linkedin.com/in/tpjoe/).