---
title: Computational Design of Materials for Fuel Cells
subtitle: Designing materials from first principles (Academic Work)
date: 2018-03-15
bigimg: [{src: "../../img/Capture.JPG", desc: "myself"}]
---

![metals](https://ars.els-cdn.com/content/image/1-s2.0-S1359645418303008-fx1.jpg)

# **Inspiration:**
Solid Oxide Fuel Cells (SOFC) operates at more than 600 °C with both very oxidative and reductive 
environment. This makes it extremely hard to find a material that can seal SOFC.
Screening of materials experimentally takes very long time. Here we employed theoretical approach and
developed a descriptor that would allow us to quickly screen for materials that will "wet" the 
SOFC walls (and hence upon cooling it can form a seal), and will be stable in both environment.
The method was done in VASP and analyzed by a Python package pymatgen.

## **Results:**
The paper can be found [here](https://www.sciencedirect.com/science/article/pii/S1359645418303008).

**Stacks used**: VASP, pymatgen





