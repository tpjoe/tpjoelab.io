---
title: Catching Fake News on Twitters
subtitle: Using Neural Network to Detect Fake Tweets!
date: 2018-04-15
tags: ["machine learning", "social media", "data science"]
bigimg: [{src: "../../img/Capture.JPG", desc: "myself"}]
---

![fake news](https://img.thedailybeast.com/image/upload/c_crop,d_placeholder_euli9k,h_562,w_1000,x_0,y_0/dpr_2.0/c_limit,w_740/fl_lossy,q_auto/v1516046503/A180115-Lorenz-Twitter-fake-news-hero_f8kfkv)

# **Inspiration:**
There's no denial that data manipulation on social media play has a huge impact
on the society. The ability to target a group of people, to generate information so easily, and
to know what those people want to hear is a dangerous tool that should be kept in check.
Fake news are so wide spread today and people perception could be easily affected by it.
Take this headline from The Hill as an example

> _"Researchers say fake news had 'substantial impact' on 2016 election" _
[- The Hill](http://thehill.com/policy/cybersecurity/381449-researchers-say-fake-news-had-substantial-impact-on-2016-election)

This project aims to tackle this data using a data-driven approach, particularly we hope to:
1. Detect patterns of how fake news spread on Twitter.
2. Build classification models to detect fake tweets.

To do this, we employed a labeled data from expert available [here](http://alt.qcri.org/$\sim$wgao/data/rumdect.zip).
Note that due to privacy policy of Twitter. The dataset only provide 

## **Results:**

An in detail analysis and methods can be found in the manuscript [here](https://github.com/tpjoe/CSE847_Machine_Learning/blob/master/Final%20Report.pdf)
You can see the graphical results [here](https://github.com/tpjoe/CSE847_Machine_Learning).

**Stacks used**: Python, sklearn, keras, tweepy





