---
title: Catching Medicare Fraud Providers from Sale Anomalies
subtitle: Data Science in Healthcare
date: 2018-06-18
tags: ["machine learning", "healthcare", "data science"]
bigimg: [{src: "/img/Capture.JPG", desc: "myself"}]
---

<p align="center">
<img src="../img/cms_logo.jpg" />
<p>

<body lang=EN-US link="#0563C1" vlink="#954F72" style='tab-interval:.5in'>

<div class=WordSection1>

<h1><b>Abstract<o:p></o:p></b></h1>

<p class=MsoNormal><b>Inspiration</b>:</p>

<p class=MsoNormal><span style='mso-spacerun:yes'> </span>In an aging society,
Medicare has becoming increasingly vital. Unfortunately, with modern US
Healthcare programs’ complexity and sophistication, fraud losses in healthcare
cost US taxpayers a staggering amount, to quote from the Justice Department, </p>

<p class=MsoNormal><span style='mso-spacerun:yes'> </span>&quot;<i>Health care
fraud costs the United States tens of billions of dollars each year. Some
estimates put the figure close to $100 billion a year. It is a rising threat,
with national health care expenditures estimated to exceed $3 trillion in 2014</i>.&quot;
</p>

<p class=MsoNormal>[- <a
href="http://www.justice.gov/criminal-fraud/health-care-fraud-unit">U.S.
Department of Justice</a>] </p>

<p class=MsoNormal>This project aims to tackle this data using a data-driven
approach, particularly we hope to: </p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;mso-list:l0 level1 lfo2'><![if !supportLists]><span
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Detect patterns of fraud Medicare providers. </p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;mso-list:l0 level1 lfo2'><![if !supportLists]><span
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Build classification models to detect these
providers. </p>

<p class=MsoNormal>Solutions to all problems start with gathering data and
seeing the big picture through big data analytics lens, here I employed a
combination of data including <a
href="https://cloud.google.com/bigquery/public-data/medicare">CMS Medicare 2014
Part D</a> data from Google <span class=SpellE>BigQuery</span> , <a
href="https://oig.hhs.gov/exclusions/exclusions_list.asp#instruct">Medicare
Exclusion list</a> from the Office of Inspector General, and a <a
href="https://simplemaps.com/data/us-cities">geographical dataset</a>. <span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal><b>Results Summary</b>: In summary, we found that fraud
providers for Medicare related drugs tend to sell more narcotics (4x more times
to be exact), with much higher price than typical providers would. Leveraging
this knowledge, PCA, and several other techniques to capture the problem's
class imbalance, we built several models to capture fraud providers and achieve
AUC of more than 0.98. </p>

<p class=MsoNormal>You can see the results and analysis <a
href="https://plot.ly/~tpjoe33/12/catching-medicare-fraud-providers-from-sale-anomalies/">here</a>.
A more detailed code and notebook is shown <a
href="https://github.com/tpjoe/SpringBoard/blob/master/Project%20I/Project%20I.ipynb">here</a>.
</p>

<p class=MsoNormal><b>Stacks used</b>: Python, <span class=SpellE>sklearn</span>,
<span class=SpellE>plotly</span>, pandas<o:p></o:p></p>

</div>

</body>


<h1><b>Detailed discussion<o:p></o:p></b></h1>

<body lang=EN-US link=blue vlink=purple style='tab-interval:.5in'>

<div class=WordSection1>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormalCxSpMiddle style='margin-left:.5in;mso-add-space:auto;
text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo2'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN style='font-size:14.0pt;line-height:115%'>Introduction<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN>In the United
States, Medicare is a single-payer, national social insurance program
administered by the U.S. federal government since 1966. Healthcare is an
increasingly important issue for many Americans; the Centers for Medicare and
Medicaid Services estimate over 41 million Americans were enrolled in Medicare
prescription drug coverage programs as of October 2016. It provides health
insurance for Americans aged 65 and older who have worked and paid into the
system through the payroll tax. Unfortunately, with modern US Healthcare
programs’ complexity and sophistication, fraud losses in healthcare cost US
taxpayers a staggering amount, to quote from the Justice Department,</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><span lang=EN>“<i>Health
care fraud costs the United States tens of billions of dollars each year. <br>
Some estimates put the figure close to $100 billion a year. It is a rising
threat, <br>
with national health care expenditures estimated to exceed $3 trillion in 2014.</i>”
<br>
- U.S. Department of Justice<br style='mso-special-character:line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN>This project aims
to tackle this data using a data-driven approach, particularly we hope to:</span></p>

<p class=MsoNormalCxSpMiddle style='margin-left:.5in;mso-add-space:auto;
text-align:justify;text-indent:-.25in;mso-list:l4 level1 lfo1'><![if !supportLists]><span
lang=EN><span style='mso-list:Ignore'>&#9679;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN>Detect patterns of fraud Medicare
providers.</span></p>

<p class=MsoNormalCxSpMiddle style='margin-left:.5in;mso-add-space:auto;
text-align:justify;text-indent:-.25in;mso-list:l4 level1 lfo1'><![if !supportLists]><span
lang=EN><span style='mso-list:Ignore'>&#9679;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN>Build classification models to
detect these providers.</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN>Solutions to all
problems start with gathering data and seeing the big picture through big data
analytics lens, here I employed a combination of data including CMS Medicare
2014 Part D data from Google BigQuery, Medicare Exclusion list from the Office
of Inspector General, and a geographical dataset. </span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN>Topics that will be
covered using these datasets include</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormalCxSpMiddle style='text-align:justify;mso-list:l0 level1 lfo3'><span
     lang=EN>Exploration of Medicare drug prescription and cost across states</span></li>
 <li class=MsoNormalCxSpMiddle style='text-align:justify;mso-list:l0 level1 lfo3'><span
     lang=EN>Exploration of excluded drug providers</span></li>
 <li class=MsoNormalCxSpMiddle style='text-align:justify;mso-list:l0 level1 lfo3'><span
     lang=EN>Feature selection and engineering</span></li>
 <li class=MsoNormalCxSpMiddle style='text-align:justify;mso-list:l0 level1 lfo3'><span
     lang=EN>Selection and optimization of classifications models</span></li>
</ol>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN>Data source: <span
style='mso-tab-count:1'>  </span>1. https://cloud.google.com/bigquery/public-data/medicare</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><span
style='mso-spacerun:yes'>                     </span><span style='mso-tab-count:
1'>   </span>2. https://oig.hhs.gov/exclusions/exclusions_list.asp#instruct</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><span
style='mso-tab-count:2'>                        </span>3. https://simplemaps.com/data/us-cities</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN>Medicare Part D
plans are offered by insurance companies and other private companies approved
by Medicare.<b style='mso-bidi-font-weight:normal'> </b><span style='mso-bidi-font-weight:
bold'>The analyses and conclusion from the current study are expected to be
helpful for just these organizations, but also those that oversee Medicare
program including <span class=st>federal centers such as </span>Department of
Justice and Department of Health and Human Services.</span><b style='mso-bidi-font-weight:
normal'><o:p></o:p></b></span></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraph style='text-align:justify;text-indent:-.25in;
mso-list:l2 level1 lfo2'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%;mso-bidi-font-family:
Arial'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN style='font-size:14.0pt;line-height:115%'>Dataset Description and
Cleaning<o:p></o:p></span></b></p>

<p class=MsoListParagraph style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><span lang=EN
style='mso-bidi-font-weight:bold'>The Medicare part D data set contains over 23
million observations of each provider with each drug (~3GB). The features
included in the dataset are npi (national provider identifier), provider city,
provider state, specialty description, description flag, drug name, generic name,
beneficiary (bene) count, total claim count, total day supply, and total drug cost
for all Medicare beneficiaries and beneficiaries whose ages are greater than
65. As shown in Table 1, we can see that the statistics of just 1/10<sup>th</sup>
of the dataset (~2.3 million observations) is similar to the whole dataset and
therefore sufficient for data exploration and developing classification model.
For the exclusion dataset from the Department of Justice, it contains the npi,
names, and location of the excluded provider. From which we will only use the
npi to label the provider in the medicare dataset whether the observation is
from a fraud provider or from typical provider.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%;mso-bidi-font-weight:bold'>Table 1. The basic statistics
of the whole dataset and the sample (1/10<sup>th</sup>) we used.<o:p></o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
 width=656 style='width:492.1pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-table-lspace:9.0pt;margin-left:6.75pt;mso-table-rspace:9.0pt;margin-right:
 6.75pt;mso-table-anchor-vertical:page;mso-table-anchor-horizontal:margin;
 mso-table-left:left;mso-table-top:286.15pt;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:solid windowtext 1.0pt;border-right:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>#
  bene<o:p></o:p></span></b></p>
  </td>
  <td width=76 style='width:57.35pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>#
  claims<o:p></o:p></span></b></p>
  </td>
  <td width=76 style='width:57.35pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>#
  days supplied <o:p></o:p></span></b></p>
  </td>
  <td width=76 style='width:57.35pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>Total
  drug cost<o:p></o:p></span></b></p>
  </td>
  <td width=69 style='width:51.7pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>#
  bene 65+<o:p></o:p></span></b></p>
  </td>
  <td width=76 style='width:57.35pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>#
  claim 65+<o:p></o:p></span></b></p>
  </td>
  <td width=76 style='width:57.35pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>#
  days supplied 65+<o:p></o:p></span></b></p>
  </td>
  <td width=77 style='width:.8in;border:solid windowtext 1.0pt;border-left:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>Total
  drug cost 65+<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:12.05pt'>
  <td width=656 colspan=9 style='width:492.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>Full
  dataset<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>count<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>8938114<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>23773930<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>23773930<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>23773930<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3262820<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>13808510<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>13808510<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>13808510<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>mean<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>28.15<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>50.64<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2030.82<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3920.42<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>19.20<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>47.19<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1991.86<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3153.68<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>std<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>34.68<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>85.27<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3664.79<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>25179.71<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>45.44<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>85.56<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3770.67<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>17027.91<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.25<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>14.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>15.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>450.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>272.41<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>13.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>390.00<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>210.79<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.50<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>19.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>24.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>900.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>728.25<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>13.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>21.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>840.00<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>632.86<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:12.05pt'>
  <td width=53 style='width:39.95pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.75<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>32.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>50.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1980.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2528.18<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>23.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>46.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1890.00<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2255.16<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:12.05pt'>
  <td width=656 nowrap colspan=9 valign=bottom style='width:492.1pt;border:
  solid windowtext 1.0pt;border-top:none;mso-border-top-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-ascii-theme-font:major-latin;
  mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:major-latin;
  mso-bidi-font-family:"Times New Roman";mso-ansi-language:EN-US'>Sampled
  dataset (1/10<sup>th</sup> of the data)<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>count<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>893987<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2378379<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2378379<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2378379<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>326805<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1381589<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1381589<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1381589<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>mean<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>28.11<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>50.57<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2027.16<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3894.34<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>19.15<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>47.14<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1988.73<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3150.29<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>std<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>31.87<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>84.78<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3650.68<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>26340.35<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>39.71<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>84.72<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>3750.46<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>17690.43<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.25<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>14.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>15.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>450.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>272.72<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>13.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>390.00<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>210.98<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:12.05pt'>
  <td width=53 style='width:39.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.50<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>19.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>24.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>900.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>728.41<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>13.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>21.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;padding:0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>840.00<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>633.53<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;mso-yfti-lastrow:yes;height:12.05pt'>
  <td width=53 style='width:39.95pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><b><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>0.75<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.1pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>32.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>50.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1980.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2529.27<o:p></o:p></span></p>
  </td>
  <td width=69 style='width:51.7pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>23.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>46.00<o:p></o:p></span></p>
  </td>
  <td width=76 style='width:57.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>1890.00<o:p></o:p></span></p>
  </td>
  <td width=77 style='width:.8in;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:12.05pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:286.15pt;mso-height-rule:exactly'><span style='font-size:
  10.0pt;font-family:"Calibri",sans-serif;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>2253.42<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><b
style='mso-bidi-font-weight:normal'><span lang=EN style='font-size:14.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.25in'><span lang=EN
style='mso-bidi-font-weight:bold'>For data wrangling steps, several data
cleaning were applied including<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph style='text-align:justify;text-indent:-.25in;
mso-list:l1 level1 lfo5'><![if !supportLists]><span lang=EN style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN style='mso-bidi-font-weight:bold'>Checking
for NaN (no NaN were found in the interested columns of the original dataset).<o:p></o:p></span></p>

<p class=MsoListParagraph style='text-align:justify;text-indent:-.25in;
mso-list:l1 level1 lfo5'><![if !supportLists]><span lang=EN style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN style='mso-bidi-font-weight:bold'>Managing
address location text data.<o:p></o:p></span></p>

<p class=MsoListParagraph style='text-align:justify;text-indent:-.25in;
mso-list:l1 level1 lfo5'><![if !supportLists]><span lang=EN style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN style='mso-bidi-font-weight:bold'>Combining
data from different datasets.<o:p></o:p></span></p>

<p class=MsoListParagraph style='text-align:justify;text-indent:-.25in;
mso-list:l1 level1 lfo5'><![if !supportLists]><span lang=EN style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN style='mso-bidi-font-weight:bold'>Grouping
data by States, drug providers, costs, etc.<o:p></o:p></span></p>

<p class=MsoListParagraph style='text-align:justify;text-indent:-.25in;
mso-list:l1 level1 lfo5'><![if !supportLists]><span lang=EN style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN style='mso-bidi-font-weight:bold'>Data
preprocessing: creating features that weren’t readily available in the data
(ratios).<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'>The raw data does not contain missing values. However, by grouping data
by drug sale. Certain providers do not have any sale, which makes a feature
drug sale per provider become infinity (NaN). These providers were removed as
they do not sell any drug anyway. There were some outliers, but we do not
delete them as we are trying to detect anomalies in sales, and these outliers
maybe the anomaly we are looking for.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph style='text-align:justify;text-indent:-.25in;
mso-list:l2 level1 lfo2'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%;mso-bidi-font-family:
Arial'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN style='font-size:14.0pt;line-height:115%'>Initial findings from
exploratory analysis<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraph style='margin-left:0in;text-align:justify;text-indent:
.5in;mso-list:l5 level2 lfo6'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%;mso-bidi-font-family:
Arial'><span style='mso-list:Ignore'>3.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN style='font-size:14.0pt;line-height:115%'>Data Story <o:p></o:p></span></b></p>

<p class=MsoListParagraph style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<div>
    <a href="https://plot.ly/~tpjoe33/0/" target="_blank" title="drug_dropdown" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/0.png" alt="drug_dropdown" style="max-width: 100%;width: 600px;"  width="400" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:0" src="https://plot.ly/embed.js" async></script>
</div>


<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%;mso-bidi-font-weight:bold'>Fig. 1. The top 10 drugs
prescribed in Medicare by total cost.<span style='mso-spacerun:yes'>  
</span><o:p></o:p></span></p>

<div>
    <a href="https://plot.ly/~tpjoe33/2/" target="_blank" title="alcohol-box-plot" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/2.png" alt="alcohol-box-plot" style="max-width: 100%;width: 500px;"  width="400" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:2" src="https://plot.ly/embed.js" async></script>
</div>


<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%;mso-bidi-font-weight:bold'>Fig. 2. The portion of Medicare 
used by 65+ beneficiaries.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span
style='mso-ansi-language:EN-US'>Medicare is the federal health insurance
program for: People who are 65 or older. Certain younger people with
disabilities. People with End-Stage Renal Disease (permanent kidney failure
requiring dialysis or a transplant, sometimes called ESRD). Medicare Part D includes
prescription drug coverage to original Medicare some Medicare Cost Plans, some
Medicare Private-Fee-for-Service Plans, Medicare Medical Savings Account Plans,
available for. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span
style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span
style='mso-ansi-language:EN-US'>By looking just at the Medicare dataset itself,
we can draw some interesting facts out of the data. In 2014, a</span><span
lang=EN> total of $ 90 billion from 83,000 providers with 2695 unique drugs
were prescribed using Medicare. By the cost of total prescriptions, the drug
with the highest total cost is Sovaldi, a medication for Hepatitis C (Fig. 1).
However, by amount of prescriptions, Hydrocodone - a narcotic pain relieving
drug - is the most prescribed drug (figure not shown). Across the States, only
24% of the beneficiaries are of 65+ ages (the rest are disabled beneficiaries).
However, beneficiaries with 65+ age contributed to more than 45% of the cost in
Medicare prescription Fig.2.</span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span
style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN>Interestingly,
there is a huge disparity in the Medicare prescription cost across the states,
with States near the West Coast tend to be significantly cheaper Fig. 3.
Although this will not be the focus of the current study, it is an excellent
starting point to understand and investigate geographic and racial and ethnic
differences in health outcomes. This information may be used to inform policy
decisions and to target populations and geographies for potential
interventions.</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></b></p>

<div>
    <a href="https://plot.ly/~tpjoe33/4/" target="_blank" title="choropleth_medicare_cost" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/4.png" alt="choropleth_medicare_cost" style="max-width: 60%;width: 400px;"  width="400" 
    onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:4" src="https://plot.ly/embed.js" async></script>
</div>


<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%;mso-bidi-font-weight:bold'>Fig. 3. The average cost of
drug prescription in different cities showing disparities across United
States.<span style='mso-spacerun:yes'></span></span><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><o:p></o:p></span></b></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-indent:.5in'><span lang=EN style='mso-bidi-font-weight:
bold'>Fraud Medicare provider is listed in an exclusion list by the Department
of Justice due to several reasons including:<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN style='mso-bidi-font-weight:bold'>1. Fraud by
claiming of Medicare reimbursement to which the claimant is not entitled.<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN style='mso-bidi-font-weight:bold'>2. Offenses
related to the delivery of items or services under Medicare.<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN style='mso-bidi-font-weight:bold'>3. Patient
abuse or neglect<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN style='mso-bidi-font-weight:bold'>4. Unlawful
manufacture, distribution, prescription, or dispensing of controlled
substances.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'>From the combination of the datasets, fraud
provider associated with the Part D Medicare accounts to approximately 0.2%, of
all providers and prescribed at least $200 million worth of drugs in the year
2014 alone. From exploratory data analyses, we found that there is no
correlation in the provider’s specialty between typical and fraud providers.
This feature of the provider is therefore not worth it for identification of
fraud providers.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'>Interestingly, by ranking the top 10 most
prescribed drugs again, but from only the fraud providers (Fig. 4), a very
unique set of drug trends are revealed. More than 5 narcotics, including
OxyContin (Fig. 5), Oxymorphone, Morphine, Oxycodone HCL, and
Oxycodone-acetaminophen are on the list compared to just 1 from all providers
shown in Fig. 1 (Hydrocodone). Suggesting that fraud providers tend to sell
significantly more narcotics and could be useful as a feature for
classification purposes. However, feature engineering needs to be applied.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<div>
    <a href="https://plot.ly/~tpjoe33/6/" target="_blank" title="drug_excluded" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/6.png" alt="drug_excluded" style="max-width: 100%;width: 600px;display: inline-block;" 
    width="600" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:6" src="https://plot.ly/embed.js" async></script>
</div>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%;mso-bidi-font-weight:bold'>Fig. 4. The top 10 drugs
prescribed by fraud providers.<span style='mso-spacerun:yes'>
</span>.</p>

<p align="center">
<img src="../img/oxycontin.jpg" width="400">
</p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%;mso-bidi-font-weight:bold'>
Fig. 5. The top selling narcotics drug (OxyContin)</span>.<sup>1<o:p></o:p></sup></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'>Furthermore, by looking into a statistic
overview of the sale of narcotics between typical and fraud drug providers, we
found a more convincing evidence. In Fig. 6, an average total sale of generic
(in blue) and narcotic (in red) drugs are shown for typical and fraud
providers. Although both typical and fraud providers sell generic drugs at
about the same cost ($3900), fraud providers’ sales of narcotics skyrocketed to
over $41,000 on average, while typical providers have narcotic sale similar to
other drugs. The total cost of narcotics by fraud providers are high primarily
due to the sale amount. The amount is depicted as a size of the bubble in Fig.
6. This difference in sale behavior between typical and fraud provider will be
further investigated in the next section; however, this shows that it is an
interesting feature that can be leveraged for machine learning models. As drug
cost is currently just one feature in the dataset, how we expand this feature
into more independent variables will be discussed in Section 4.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<div>
    <a href="https://plot.ly/~tpjoe33/8/" target="_blank" title="provider-behavior" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/8.png" alt="provider-behavior" style="max-width: 100%;width: 600px;"  width="600" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:8" src="https://plot.ly/embed.js" async></script>
</div>


<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 6.
Average total Medicare cost per beneficiary by excluded (fraud) and
non-excluded (typical) providers.<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'><span
style='mso-tab-count:1'>          </span>3.2 Inferential Statistics<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'>In this section, we provide more statistical supports to the conclusion
drawn from data analysis in the previous section.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p align="center">
<img src="../img/scatter.jpg" width="700">
</p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 7.
Average amount of generic drugs (left) and narcotics (right) plotted against
the average total sale (in $) of typical (light blue) and fraud providers
(red).<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormalCxSpMiddle style='mso-list:l3 level1 lfo4'><b
     style='mso-bidi-font-weight:normal'><span lang=EN>Sale Behaviors<o:p></o:p></span></b></li>
</ol>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN>Here,
we will look more into the statistics of this anomalies. The scatter plot of
the drug/narcotics amount and cost sale from typical and fraud providers in
Fig. 7 showed that there is a huge overlaps and definitely inseparable.
However, we can observe that in narcotics only plot (Fig. 7 right), the trend
is more slightly more scattered. To determine that population’s mean of the
cost from fraud and typical providers are in fact significantly different, we
applied one-way ANOVA test. </span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN>The
results showed that the null hypothesis that fraud providers sell all drugs
with the similar total cost as typical provider has a p-value of 0.6987,
therefore, we do NOT reject the hypothesis. On the other hand, the null
hypothesis that fraud providers sell narcotics with the same total cost as
typical provider has a p-value of 0.0000, therefore, we REJECT the hypothesis.
This supports that there is a difference on average; however, as can be seen in
Fig. 7 that using this feature (total sale) directly does not suffice for any
model to separate it. Therefore, we need increase the dimension of the data by
constructing more independent variables out of the existing information, which
will be discussed in the Section 4.</span></p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN style='font-size:8.0pt;line-height:115%'>*narcotics
includes ['OXYCONTIN', 'OXYMORPHONE HCL ER', 'MORPHINE SULFATE ER', 'OXYCODONE
HCL', 'OXYCODONE-ACETAMINOPHEN', 'FENTORA', 'SUBSYS',
'HYDROCODONE-ACETAMINOPHEN', 'SUBOXONE', 'OPANA ER',
'HYDROCODONE-ACETAMINOPHEN'], which are among the top 25 of the most sold drugs
by fraud providers<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormalCxSpMiddle style='mso-list:l3 level1 lfo4'><b
     style='mso-bidi-font-weight:normal'><span lang=EN>Specialty of Fraud
     Providers</span></b></li>
</ol>

<p class=MsoNormal align=center style='text-align:center'><span lang=EN><span
style='mso-spacerun:yes'>              </span></span></p>

<div>
    <a href="https://plot.ly/~tpjoe33/10/" target="_blank" title="stacked-bar" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/10.png" alt="stacked-bar" style="max-width: 100%;width: 600px;" 
    width="600" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:10" src="https://plot.ly/embed.js" async></script>
</div>


<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 8.
The percentages of excluded (fraud) and non-excluded (typical) Medicare
providers’ specialties.<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN>The
second objective is to determine if there is any correlation between the
specialty of providers and whether that provider is a fraud one. For this
objective, we employed Spearman’s correlation because it is suitable for
comparison between two categorical classes and it does not assume the datasets
are normally distributed. The correlation between providers' specialty and
whether the provider is fraud is 0.0023 with a p-value of 0.0003. The low
p-value indicates that the null hypothesis that specialty and whether the
providers are fraud are uncorrelated, is rejected. However, the correlation
value was virtually 0, which means that the correlation would not be
necessarily useful, and hence we would not leverage this feature for the
classification model.</span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'>4. Results and
In-depth analysis using machine learning<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><span style='mso-tab-count:1'>            </span>Prior to employing
machine learning models. We performed several feature engineering to the
original data. First, we need to create more independent variables out of the
current useful features, drug name and total sale. We expanded these two
features by creating a matrix with providers as indices and each column of the
row is a drug name with a value of sale portion of that drug. In this way, we
construct independent features that reflect sale behavior of each provider. In
should be noted that since there are more than 2 million providers. It does not
make sense to use all drugs as a part of feature construction. Herein, we only
used drugs that were sold (at least once) by fraud providers. This reduces the
number of unique drugs from 2191 to 553 types. The example of the resulting
table is shown in Table 2.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Table
2. Example of the features generated from drug names and its sale portion for
different providers.<o:p></o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=502
 style='width:376.15pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:29.7pt'>
  <td width=65 style='width:48.95pt;border-top:solid windowtext 1.0pt;
  border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:29.7pt'></td>
  <td width=50 style='width:37.45pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:29.7pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>ABILIFY<o:p></o:p></span></b></p>
  </td>
  <td width=78 style='width:58.35pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:29.7pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>ACEBUTOLOL HCL<o:p></o:p></span></b></p>
  </td>
  <td width=98 style='width:73.6pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:29.7pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>ACETAMINOPHEN-CODEINE<o:p></o:p></span></b></p>
  </td>
  <td width=59 style='width:43.9pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:29.7pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>ACTONEL<o:p></o:p></span></b></p>
  </td>
  <td width=68 style='width:51.15pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:29.7pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>ACYCLOVIR<o:p></o:p></span></b></p>
  </td>
  <td width=62 style='width:46.2pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:29.7pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>ADEFOVIR DIPIVOXIL<o:p></o:p></span></b></p>
  </td>
  <td width=22 style='width:16.55pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:29.7pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>...<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:15.0pt'>
  <td width=65 style='width:48.95pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>npi<o:p></o:p></span></b></p>
  </td>
  <td width=50 style='width:37.45pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'></td>
  <td width=78 style='width:58.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'></td>
  <td width=98 style='width:73.6pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'></td>
  <td width=59 style='width:43.9pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'></td>
  <td width=68 style='width:51.15pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'></td>
  <td width=62 style='width:46.2pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'></td>
  <td width=22 style='width:16.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'></td>
 </tr>
 <tr style='mso-yfti-irow:2;height:15.0pt'>
  <td width=65 style='width:48.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1003000126<o:p></o:p></span></b></p>
  </td>
  <td width=50 style='width:37.45pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=78 style='width:58.35pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=98 style='width:73.6pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=59 style='width:43.9pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=68 style='width:51.15pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.2pt;border:none;mso-border-top-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=22 style='width:16.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal style='line-height:normal'><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>...<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:15.0pt'>
  <td width=65 style='width:48.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1003000142<o:p></o:p></span></b></p>
  </td>
  <td width=50 style='width:37.45pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=78 style='width:58.35pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=98 style='width:73.6pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=59 style='width:43.9pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=68 style='width:51.15pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=22 style='width:16.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal style='line-height:normal'><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>...<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:15.0pt'>
  <td width=65 style='width:48.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1003000167<o:p></o:p></span></b></p>
  </td>
  <td width=50 style='width:37.45pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=78 style='width:58.35pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=98 style='width:73.6pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.01<o:p></o:p></span></p>
  </td>
  <td width=59 style='width:43.9pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=68 style='width:51.15pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=22 style='width:16.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal style='line-height:normal'><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>...<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:15.0pt'>
  <td width=65 style='width:48.95pt;border:none;border-left:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1003000407<o:p></o:p></span></b></p>
  </td>
  <td width=50 style='width:37.45pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=78 style='width:58.35pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=98 style='width:73.6pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=59 style='width:43.9pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=68 style='width:51.15pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=22 style='width:16.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal style='line-height:normal'><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>...<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;mso-yfti-lastrow:yes;height:15.0pt'>
  <td width=65 style='width:48.95pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1003000423<o:p></o:p></span></b></p>
  </td>
  <td width=50 style='width:37.45pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=78 style='width:58.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=98 style='width:73.6pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=59 style='width:43.9pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=68 style='width:51.15pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=62 style='width:46.2pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.0pt'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=22 style='width:16.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.0pt'>
  <p class=MsoNormal style='line-height:normal'><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>...<o:p></o:p></span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%'><span style='mso-tab-count:1'>                </span></span><span
lang=EN>As one might have expected, the generated features that consist of 553
columns are low-rank matrix and is filled mostly with zeros. To get rid of
unnecessary features as well as avoiding the curse of dimensionalities, we
applied principal component analysis (PCA) to the data. In Fig. 9, it is shown
that by using 2 and 3 components, the variances explained are 8% and 10%
respectively. This may sound like a small number, but consider there are more
than 553 features, this means that the rest of the 550 features would
contribute to less than 0.17% each. This implies that the rest of the
components may be negligible.</span></p>

<p align="center">
<img src="../img/variance_exp.jpg" width="500">
</p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 9.
Explained variance using 2 and 3 principal components.<o:p></o:p></span></p>


<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><span style='mso-tab-count:1'>            </span>To test if the
engineered data could be better separated than the original ones as shown in
Fig. 7 (right), scatter plots of the data with 2 and 3 components with label
are shown in Fig. 10. In both of these figures, we can observe the
agglomeration of the fraud providers, although not totally separated from
typical providers. In the case where only 2 principal components were used,
majority of the fraud providers exhibited PC2 lower than 0.1, with significant
amount of those data points lying in a negative PC1 and negative PC2 region. Similarly,
in the case of 3 components we can see that most fraud providers exhibited PC2
of lower than 0.2 and significant amount of those data points lying in a
negative PC1 and negative PC3 region. This shows a promising result that
machine learning models may be able to classify these points. However, judging
from the shape and degree of overlaps of the data, it is expected that decision
tree-based algorithms would do well in this situation. Not only because the
data is highly non-linear, non-Gaussian, but the amount of red data point
(fraud providers) is significantly less than the light blue ones (typical providers).<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p align="center">
<img src="../img/after_pca.jpg" width="700">
</p>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 10.
Labelled scatter plot of typical (light blue) and fraud (red) providers plotted
in its corresponding 2 principal components (left) or 3 principal components
(right).<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'>In summary, the number of typical providers
is 486675 providers, as opposed to just 566 fraud providers. This would a heavy
class imbalance for our classification model that even a tree-based algorithm
may not be able to identify. To address this problem, we investigated several bootstrapping
technique including up-sampling, down-sampling, and combination of both using
an ensemble method, random forest with 50 trees, as a benchmarking algorithm. The
bootstrapped samples will be separated 66/33 for training and test set. The
results are then judged based on confusion matrices. It should be noted here
that simple accuracy is not used as a scoring metric because even if the
classification model predict that all providers are typical, the accuracy will
still be above 98%. Using confusion matrix, which encompass both true/false
positive and negative, enable us to clearly see where the model performs best
and which needs to be improved.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='font-size:
9.0pt;line-height:115%'>Table 3. The confusion matrix results with precision
and recall score for no and different types of bootstrapping methods. The
results are obtained using random forest algorithm with 50 trees.</span><span
lang=EN style='mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=631
 style='width:472.9pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:.2in'>
  <td width=50 nowrap valign=bottom style='width:37.7pt;border-top:solid windowtext 1.0pt;
  border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'></td>
  <td width=76 nowrap valign=bottom style='width:57.1pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'></td>
  <td width=128 nowrap colspan=2 style='width:95.65pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>No Resampling<o:p></o:p></span></b></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Up-sampling 10x<o:p></o:p></span></b></p>
  </td>
  <td width=134 nowrap colspan=2 style='width:100.25pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Down-sampling 10x<o:p></o:p></span></b></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Combined<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:.2in'>
  <td width=126 nowrap colspan=2 valign=bottom style='width:94.8pt;border-top:
  none;border-left:solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal style='line-height:normal'><b><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>Typical
  providers<o:p></o:p></span></b></p>
  </td>
  <td width=128 nowrap colspan=2 style='width:95.65pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>486675<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 valign=bottom style='width:91.1pt;border:none;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>486675<o:p></o:p></span></p>
  </td>
  <td width=134 nowrap colspan=2 style='width:100.25pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>48670<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>48670<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:.2in'>
  <td width=126 nowrap colspan=2 valign=bottom style='width:94.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal style='line-height:normal'><b><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'>Fraud
  providers<o:p></o:p></span></b></p>
  </td>
  <td width=128 nowrap colspan=2 style='width:95.65pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>566<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 valign=bottom style='width:91.1pt;border-top:
  none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>5480<o:p></o:p></span></p>
  </td>
  <td width=134 nowrap colspan=2 style='width:100.25pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>566<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>5480</span><span style='font-size:9.0pt;mso-fareast-font-family:
  "Arial Unicode MS";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:.2in'>
  <td width=126 nowrap colspan=2 rowspan=2 style='width:94.8pt;border-top:none;
  border-left:solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:16.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Train</span></b><span style='font-size:9.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.1pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid #BFBFBF .5pt;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>326078<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid #BFBFBF .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-bottom-alt:solid #BFBFBF .5pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid #BFBFBF .5pt;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormalCxSpMiddle align=center style='text-align:center'><span
  lang=EN style='font-size:9.0pt;line-height:115%;mso-fareast-font-family:"Arial Unicode MS";
  color:black'>326054<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid #BFBFBF .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-bottom-alt:solid #BFBFBF .5pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>3<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.1pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid #BFBFBF .5pt;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>32608<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.15pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid #BFBFBF .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-bottom-alt:solid #BFBFBF .5pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid #BFBFBF .5pt;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>32553<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid #BFBFBF .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-bottom-alt:solid #BFBFBF .5pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>76<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:.2in'>
  <td width=67 nowrap style='width:50.1pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>116<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>257<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>757<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>2929<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.1pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>93<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.15pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>287<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>635<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>3016<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:.2in'>
  <td width=126 nowrap colspan=2 style='width:94.8pt;border-top:none;
  border-left:solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Prec.<o:p></o:p></span></p>
  </td>
  <td width=128 colspan=2 style='width:95.65pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 valign=bottom style='width:91.1pt;border:none;
  border-right:solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.9999<o:p></o:p></span></p>
  </td>
  <td width=134 nowrap colspan=2 style='width:100.25pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.9754<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:.2in'>
  <td width=126 nowrap colspan=2 style='width:94.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Recall<o:p></o:p></span></p>
  </td>
  <td width=128 nowrap colspan=2 style='width:95.65pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.6890<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 valign=bottom style='width:91.1pt;border-top:
  none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.7946<o:p></o:p></span></p>
  </td>
  <td width=134 nowrap colspan=2 style='width:100.25pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.7553<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.8261<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:.2in'>
  <td width=126 nowrap colspan=2 rowspan=2 style='width:94.8pt;border-top:none;
  border-left:solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
  style='font-size:16.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Test</span></b><span style='font-size:14.0pt;
  mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.1pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-top-alt:windowtext;
  mso-border-left-alt:windowtext;mso-border-bottom-alt:#BFBFBF;mso-border-bottom-themecolor:
  background1;mso-border-bottom-themeshade:191;mso-border-right-alt:#BFBFBF;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>160596<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap valign=bottom style='width:45.55pt;border-top:none;
  border-left:none;border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:
  background1;mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:windowtext;mso-border-left-alt:#BFBFBF;mso-border-left-themecolor:
  background1;mso-border-left-themeshade:191;mso-border-bottom-alt:#BFBFBF;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:windowtext;mso-border-style-alt:solid;mso-border-width-alt:
  .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>1<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-top-alt:windowtext;
  mso-border-left-alt:windowtext;mso-border-bottom-alt:#BFBFBF;mso-border-bottom-themecolor:
  background1;mso-border-bottom-themeshade:191;mso-border-right-alt:#BFBFBF;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>160586<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:windowtext;mso-border-left-alt:#BFBFBF;mso-border-left-themecolor:
  background1;mso-border-left-themeshade:191;mso-border-bottom-alt:#BFBFBF;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:windowtext;mso-border-style-alt:solid;mso-border-width-alt:
  .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>32<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.1pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-top-alt:windowtext;
  mso-border-left-alt:windowtext;mso-border-bottom-alt:#BFBFBF;mso-border-bottom-themecolor:
  background1;mso-border-bottom-themeshade:191;mso-border-right-alt:#BFBFBF;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>16055<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.15pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:windowtext;mso-border-left-alt:#BFBFBF;mso-border-left-themecolor:
  background1;mso-border-left-themeshade:191;mso-border-bottom-alt:#BFBFBF;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:windowtext;mso-border-style-alt:solid;mso-border-width-alt:
  .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>7<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid #BFBFBF 1.0pt;mso-border-right-themecolor:
  background1;mso-border-right-themeshade:191;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-top-alt:windowtext;
  mso-border-left-alt:windowtext;mso-border-bottom-alt:#BFBFBF;mso-border-bottom-themecolor:
  background1;mso-border-bottom-themeshade:191;mso-border-right-alt:#BFBFBF;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-style-alt:solid;mso-border-width-alt:.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>15924<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;mso-border-bottom-themecolor:background1;
  mso-border-bottom-themeshade:191;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:windowtext;mso-border-left-alt:#BFBFBF;mso-border-left-themecolor:
  background1;mso-border-left-themeshade:191;mso-border-bottom-alt:#BFBFBF;
  mso-border-bottom-themecolor:background1;mso-border-bottom-themeshade:191;
  mso-border-right-alt:windowtext;mso-border-style-alt:solid;mso-border-width-alt:
  .5pt;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>117<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:.2in'>
  <td width=67 nowrap style='width:50.1pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>193<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap valign=bottom style='width:45.55pt;border:none;
  border-right:solid windowtext 1.0pt;mso-border-top-alt:solid #BFBFBF .5pt;
  mso-border-top-themecolor:background1;mso-border-top-themeshade:191;
  mso-border-left-alt:solid #BFBFBF .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:191;mso-border-top-alt:solid #BFBFBF .5pt;
  mso-border-top-themecolor:background1;mso-border-top-themeshade:191;
  mso-border-left-alt:solid #BFBFBF .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:191;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>415<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>1379<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.1pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>185<o:p></o:p></span></p>
  </td>
  <td width=67 nowrap style='width:50.15pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>1<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid #BFBFBF 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid #BFBFBF .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:191;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>314<o:p></o:p></span></p>
  </td>
  <td width=61 nowrap style='width:45.55pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-top-alt:solid #BFBFBF .5pt;mso-border-top-themecolor:background1;
  mso-border-top-themeshade:191;mso-border-left-alt:solid #BFBFBF .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:191;
  mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=right style='text-align:right;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Arial Unicode MS";color:black;
  mso-ansi-language:EN-US'>1515<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:.2in'>
  <td width=126 nowrap colspan=2 style='width:94.8pt;border-top:none;
  border-left:solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Prec.<o:p></o:p></span></p>
  </td>
  <td width=128 colspan=2 style='width:95.65pt;border:none;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-right-alt:solid windowtext .5pt;background:#EEECE1;mso-background-themecolor:
  background2;padding:0in 5.4pt 0in 5.4pt;height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.9773<o:p></o:p></span></p>
  </td>
  <td width=134 nowrap colspan=2 style='width:100.25pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.1250<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border:none;border-right:
  solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.9283<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;mso-yfti-lastrow:yes;height:.2in'>
  <td width=126 nowrap colspan=2 style='width:94.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;background:
  #EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>Recall<o:p></o:p></span></p>
  </td>
  <td width=128 nowrap colspan=2 style='width:95.65pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.7687<o:p></o:p></span></p>
  </td>
  <td width=134 nowrap colspan=2 style='width:100.25pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0<o:p></o:p></span></p>
  </td>
  <td width=121 nowrap colspan=2 style='width:91.1pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  background:#EEECE1;mso-background-themecolor:background2;padding:0in 5.4pt 0in 5.4pt;
  height:.2in'>
  <p class=MsoNormal align=center style='text-align:center;line-height:normal'><span
  style='font-size:9.0pt;mso-fareast-font-family:"Times New Roman";color:black;
  mso-ansi-language:EN-US'>0.8283<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><span style='mso-tab-count:1'>            </span>From Table 2, we can see
that bootstrapping to increase the number of fraud providers is an important
step. On the major column, we can see that with no resampling at all, the
random forest algorithm can only capture the fraud provider in the training
set, while it utterly failed to detect a single fraud provider in the test set.
By up-sampling the fraud providers by about 10 folds, the algorithm performed
significantly better, with a very precision of 98% and satisfactory recall of
77% in the test set, which is also very close to the training set results.
However, down sampling data by ten times (down-sampling the number of typical
providers) did not yield improved results. On the contrary, the outcomes are
slightly below no sampling results. It should be noted here that down-sampling
further to 100 folds were investigated and the results exacerbate. Lastly, the
results from combination of these two techniques showed an improved in recall,
but lower precision compared to up-sampling only method.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'>For the present study, we favor a
conservative approach and choose to perform only up-sampling. We chose to
maintain high-precision over improved recall because we want to be able to
identify with high precision which providers are fraud. Moreover, lower
precision means <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'>significant more numbers of typical providers will be classified as fraud
providers since there are way more of them than fraud providers. This would
make verification process troublesome.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>


<div>
    <a href="https://plot.ly/~tpjoe33/13/" target="_blank" title="F1models" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/13.png" alt="F1models" style="max-width: 100%;width: 600px;"  
    width="600" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:13" src="https://plot.ly/embed.js" async></script>
</div>



<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 11.<span style='mso-spacerun:yes'>  </span>F1 score from different
classification models.<span style='mso-spacerun:yes'>     </span><o:p></o:p></span></p>

<div>
    <a href="https://plot.ly/~tpjoe33/17/?share_key=LqID8CH4ubAmXNNQNMwuPZ" target="_blank" title="ROCs" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/17.png?share_key=LqID8CH4ubAmXNNQNMwuPZ" alt="ROCs" style="max-width: 100%;width: 600px;"  
    width="600" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:17" sharekey-plotly="LqID8CH4ubAmXNNQNMwuPZ" src="https://plot.ly/embed.js" async></script>
</div>

<p class=MsoNormal><span lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 12. Receiver
operating characteristics (ROC) and area under curve (AUC) of each classification model.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><span style='mso-tab-count:1'>            </span>We first use data with 2
principal components and 5 fold cross validations to screen for the potential
classifiers. In this step we used two criteria to judge the performance of
classification models. These include F1 score and area under curve (AUC) of the
receiver operating characteristics (ROC). Both of which, unlike accuracy, are
metrics that take into account the effect of true/false positive and negative
results. F1 score is defined as 2*(precision * recall) / (precision + recall),
whereas ROC is the curve between false positive rate and true positive rate.
Therefore, AUC is equal to the probability that a classifier will rank a
randomly chosen positive instance higher than a randomly chosen negative one
&#8210; the higher the better.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'>The results in Fig. 11 and Fig. 12 showed
that random forest and extra trees algorithm performed the best among all
studied models including naïve Bayes, K nearest neighbors, Gaussian SVC,
Adaboost (decision tree base classifier), gradient boosting. Naïve Bayes, SVC,
and Adaboost were not able to capture recall and hence the F1 score becomes
zero (Fig. 11). Note that most models that yielded descent scores are based on
&quot;decision tree algorithms&quot;. This is because these trees are able to
cope with class imbalance better by their hierarchical structure that allows
them to learn signals from both classes given sufficient branches.<sup>2</sup> The
results showed that extra trees method is slightly better than random forest
with F1 and AUC of 0.873 and 0.98 versus 0.869 and 0.98 from random forest.
Here, it is worth mentioning the difference between these two methods. Random
forest is a commonly used ensemble method for many applications and it
functions by, as the name suggest, having numbers of trees each takes in a
different number of features and vote. The process is repeated with bootstrap
sampling so that each tree is exposed to more data. Extra trees, short from
extremely randomized tree, drops the idea of using bootstrapping copies, and
with the split chosen at random from the range of values in the sample at each
split, resulting in higher number of leaves. It is expected that extra trees
tend to perform worse when there is a high number of noisy features (in high
dimensional data-sets).<sup>3<o:p></o:p></sup></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><sup><span
lang=EN style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></sup></p>

<div>
    <a href="https://plot.ly/~tpjoe33/15/" target="_blank" title="PCAEffects" style="display: block; text-align: center;">
    <img src="https://plot.ly/~tpjoe33/15.png" alt="PCAEffects" style="max-width: 100%;width: 600px;"  
    width="600" onerror="this.onerror=null;this.src='https://plot.ly/404.png';" /></a>
    <script data-plotly="tpjoe33:15" src="https://plot.ly/embed.js" async></script>
</div>



<p class=MsoNormal align=center style='text-align:center;text-indent:.5in'><span
lang=EN style='font-size:9.0pt;line-height:115%'>Fig. 13.<span
style='mso-spacerun:yes'>  </span>F1 score of optimized extra tree model with
increased number of principal components.</span><span lang=EN style='mso-bidi-font-weight:
bold'><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-indent:.5in'><span lang=EN
style='mso-bidi-font-weight:bold'>To further increase the performance, we
optimized the extra trees method using grid search over different parameters
including number of trees, number of features in each tree, number of samples
required to split nodes. The optimized model required up to 200 trees instead
of just 50 trees in the original extra tree model. Lastly, the results were
optimized by increasing the number of principal components. We can see that
just 3 components are sufficient to obtain an F1 score of 0.874 (with 0.986
precision and 0.784 recall), which is considerably high.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><span style='mso-tab-count:1'>            </span>For future studies,
there are several ways that could be used to improve the performance of the
model. For example, by incorporating Medicare Provider Utilization and Payment
Data (https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports
/Medicare-Provider-Charge-Data/Part-D-Prescriber.html), which contains
information about which providers were paid by which drug companies and by how
much in order to promote their drug sales. This is required by law. Unfortunately,
the identifier provided in the payment dataset is purposefully altered so that
one could not match with the identifier in the Medicare and exclusion dataset
(npi). Although there are venues that one could back track these numbers, the
process is heavily time consuming and potentially illegal. Therefore, this
study did not pursue the use of payment dataset to add more features to it.
Applying graph theory to these would likely yield very interesting insight as
well.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'>5. Conclusion<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><span style='mso-tab-count:1'>            </span></span><span lang=EN
style='font-size:10.0pt;line-height:115%;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><span style='mso-tab-count:1'>            </span>In conclusion, the
present studies employ multiple big dataset in Medicare prescription to explore
the Medicare drug prescription in the U.S. More importantly, we highlighted the
difference in sale behaviors between typical providers and fraud providers.
Fraud providers sells drugs in different ratios and total cost from typical
providers. They tend to sell more narcotics at higher cost. In total, health
care fraud costs the United States tens of billions of dollars each year. To
detect these fraud drug providers, we performed feature engineering and
screened different classification models. The random forest and extra trees
results were found to be successful due to their bootstrapping and hierarchical
structure that enable them to capture class imbalance better. The optimized
extra trees model yielded F1 score of over 0.874 (with 0.986 precision and 0.784
recall). Further improvement in the future could be obtained by incorporating
currently prohibited payment data.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN style='mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=EN style='font-size:14.0pt;line-height:115%'>6. References<o:p></o:p></span></b></p>

<p class=MsoNormal style='line-height:normal'><span lang=EN style='mso-fareast-font-family:
"Times New Roman"'>1.
https://www.cbsnews.com/news/purdue-oxycontin-maker-to-stop-promoting-drug-to-doctors-amid-opioid-epidemic/<span
style='mso-spacerun:yes'>  </span>Access: Jun 2018<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:normal'><span style='mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US'>2. Liaw, A., &amp; Wiener, M.
(2002). Classification and regression by random forest. <i>R news</i>, <i>2</i>(3),
18-22.<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:normal'><span style='mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US'>3. Geurts, P., Ernst, D., &amp;
Wehenkel, L. (2006). Extremely randomized trees. <i>Machine learning</i>, <i>63</i>(1),
3-42.<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:normal'><span style='mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='mso-ansi-language:
EN-US;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

</div>

</body>
