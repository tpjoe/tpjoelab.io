---
title: Statistical Prediction of Lignin Polymer Yields
subtitle: Using Bayesian Inference in Polymer (Academic Work)
date: 2018-01-07
bigimg: [{src: "/img/Capture.JPG", desc: "myself"}]
---

![lignin](https://chemindigest.com/wp-content/uploads/2018/02/Researchers-develop-binder-for-lignin-based-paints.png)

# **Inspiration:**
Ever wonder why woods are brown? Well that's because of the polymer in them called
lignin. In the next generation biofuels, chemicals and fuels are extracted from 
non-food biomass like poplar, switchgrass, etc. However, unlike corn, these biomass contain
a polymer called lignin which inhibits accessibility of chemicals/enzymes to get into
the to-be sugar source. These wood are currently treated chemically to removed lignin,
from which the lignin is simply burnt for energy purposes. These lignin couldn't be used
for anything else because it  was severely degraded from the process. However, lignin contributes up to
30% of the biomass, it'd be hard to make something economically feasible if you are discarding 30% of the
raw materials.

In this work we combined experiements, statistical methods, and probablistic theory
to quantitatively show what impact the common isolation treatment has on lignin. We 
predicted precisely the amount of theoretical products that can be generated if
the quality of lignin is improved by different processes. This is an important step toward understaning
the impact of the process in order to come up with a pretreatment that would not just isolate lignin,
but isolate it mildly!

You can see the results and analysis [here](http://pubs.rsc.org/-/content/articlelanding/2017/gc/c7gc02023f/unauth#!divAbstract)

**Stacks used**: MATLAB





