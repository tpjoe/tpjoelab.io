---
title: About me
comments: false
---

![mypic](https://i1.rgstatic.net/ii/profile.image/278935989440512-1443514930249_Q512/Thanaphong_Phongpreecha.jpg)

My name is Joe. I am currently a Pd.D. student in Chemical Engineering and 
Material Science at Michigan State University. My research deals mostly with 
computational study of materials and polymer, using both ab-initio methods and
machine learning. On my side projects, I also used machine learning to work with
something more relatable to life, haha, like social media and healthcare. I am 
proficient in Python and familiar with MATLAB, C++, and SQlite.

Feel free to contact me at tp.joe@outlook.com if any of the topics in the page
interests you.

<div align=center><a href='https://www.counter12.com'><img src='https://www.counter12.com/img-00502b55ZZW4x2w9-26.gif' 
border='0' alt='counter'></a><script type='text/javascript' src='https://www.counter12.com/ad.js?id=00502b55ZZW4x2w9'>
</script></div>